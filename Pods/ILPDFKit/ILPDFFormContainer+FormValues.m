//
//  ILPDFFormContainer+FormValues.m
//  ILPDFKit
//
//  Created by Gagandeep Kaur Swaitch on 21/5/18.
//

#import "ILPDFFormContainer+FormValues.h"

@implementation ILPDFFormContainer (FormValues)

-(NSDictionary *)getFormValues
{
    NSMutableDictionary *formValues;
    formValues = [NSMutableDictionary dictionary];
    
    for (ILPDFForm *form in self)
    {
        if (form.value != nil)
        {
            [formValues setObject:form.value forKey:form.name];
        }
    }
    
    return [formValues copy];
}
@end
