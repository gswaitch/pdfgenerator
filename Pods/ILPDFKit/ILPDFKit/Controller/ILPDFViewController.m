// ILPDFViewController.m
//
// Copyright (c) 2017 Derek Blair
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <ILPDFKit/ILPDFKit.h>
#import "ILPDFFormContainer.h"
#import <ILPDFKit/ILPDFKit-Swift.h>
#import "ILPDFSignatureController.h"
#import "ILPDFFormSignatureField.h"

@interface ILPDFViewController(Private) <UITextFieldDelegate, NSURLConnectionDelegate,ILPDFSignatureControllerDelegate>
- (void)loadPDFView;
@property (nonatomic, strong) ILPDFView *pdfView;

@end


@implementation ILPDFViewController
CGFloat animatedDistance;
CGFloat keyboardHeight;
ILPDFView *pdfView;
ILPDFSignatureController *signatureController;
ILPDFFormSignatureField *signatureField;
UIScrollView *scrollV;

#pragma mark - UIViewController

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        [self loadPDFView];
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor whiteColor];
   
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChangeFrame:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showSignatureViewController:)
                                                 name:@"SignatureNotification"
                                               object:nil];
    
}


- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

#pragma mark - KVO

- (void) showSignatureViewController:(NSNotification *) notification
{
    if ([notification.object isKindOfClass:[ILPDFFormSignatureField class]])
    {
        signatureField = notification.object;
    }
    ILPDFSignatureController *vc;
    
    vc = [ILPDFSignatureController alloc];
    vc.expectedSignSize = signatureField.frame.size;
    vc.delegate = self;
    UINavigationController *popOverNavigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    [popOverNavigationController setModalPresentationStyle:UIModalPresentationFormSheet];
    [self.navigationController presentViewController:popOverNavigationController animated:YES completion:nil];
    
}


#pragma mark - Signature Controller Delegate

- (void) signedWithImage:(UIImage*) signatureImage
{
    [signatureField removeButtonTitle];
    signatureField.signatureImage.image = signatureImage;
    [signatureField informDelegateAboutNewImage];
    signatureField = nil;
}


#pragma mark - ILPDFViewController

#pragma mark - Setting the Document
- (void)setDocument:(ILPDFDocument *)document
{
    _document = document;
    [self loadPDFView];
}


#pragma mark - Relaoding the Document
- (void)reload
{
    [_document refresh];
    [self loadPDFView];
}


// Override to customize constraints.
- (void)applyConstraintsToPDFView
{
    [_pdfView pinToSuperview:UIEdgeInsetsZero guide:self.view.layoutMarginsGuide];
}


#pragma mark - Private

- (void)loadPDFView
{
    if (_pdfView.superview != nil)
    {
        [_pdfView removeFromSuperview];
    }
    _pdfView = [[ILPDFView alloc] initWithDocument:_document];
    [self.view addSubview:_pdfView];
    [self applyConstraintsToPDFView];
}


- (void)keyboardWillChangeFrame:(NSNotification *)notification
{
    UITextField *currentTextField = (UITextField *)[self.pdfView activePDFTextField];
    CGRect activeTextFieldFrame = currentTextField.frame;
    CGRect textFieldFrameInWindow = [self.view.window convertRect:activeTextFieldFrame fromView:currentTextField.superview];
    CGRect keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = keyboardRect.size.height;
    Boolean viewsOverlap = CGRectIntersectsRect(textFieldFrameInWindow, keyboardRect);
    keyboardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    double distancetoBottom = self.pdfView.frame.size.height - (textFieldFrameInWindow.origin.y) - textFieldFrameInWindow.size.height;
    double collapseDistance = keyboardRect.origin.y - distancetoBottom;
    scrollV = self.pdfView.pdfView.scrollView;
    
    if(viewsOverlap)
    {
        [UIView animateWithDuration:0.3f
                         animations:^{
                             if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
                                 
                                 UIEdgeInsets insets = UIEdgeInsetsMake(scrollV.contentInset.top, 0, collapseDistance+80, 0);
                                 scrollV.contentInset = insets;
                                 scrollV.scrollIndicatorInsets = insets;
                                 scrollV.contentOffset = CGPointMake(scrollV.contentOffset.x, scrollV.contentOffset.y+collapseDistance+80);
                                 
                             }
                             else
                             {
                                 UIEdgeInsets insets = UIEdgeInsetsMake(scrollV.contentInset.top, 0, collapseDistance-keyboardHeight+10, 0);
                                 scrollV.contentInset = insets;
                                 scrollV.scrollIndicatorInsets = insets;
                                 scrollV.contentOffset = CGPointMake(scrollV.contentOffset.x, scrollV.contentOffset.y+collapseDistance-keyboardHeight);
                             }
                             
                         }
                         completion:nil];
    }
}


- (void)keyboardDidHide: (NSNotification *) notif
{
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         UIEdgeInsets insets = UIEdgeInsetsMake(scrollV.contentInset.top, 0, 0, 0);
                         scrollV.contentInset = insets;
                         scrollV.scrollIndicatorInsets = insets;
                         scrollV.contentOffset = CGPointMake(scrollV.contentOffset.x, scrollV.contentOffset.y);
                         self.pdfView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
                     }
                     completion:nil];
}


@end

