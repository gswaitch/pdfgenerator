//
//  NSString+Utils.m
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 18/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "NSString+Utils.h"

@implementation NSString (Utils)

- (BOOL)isEmpty
{
    return [[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""];
}

@end
