//
//  CIPDFViewController.h
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 21/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CIPDFController.h"

@interface CIPDFViewController : UIViewController
@property (strong,nonatomic) NSMutableArray *imagesTaken;
@property (strong,nonatomic) NSMutableArray *signaturesCollected;
@property (nonatomic, strong) NSArray *finalSignatures;
@property (nonatomic, strong) NSArray *finalImages;
@property (nonatomic, strong) NSMutableArray *signaturesWithNameAndDate;
@property (nonatomic, assign) int numberOfImagesPerPage;
@property (strong,nonatomic) ILPDFViewController *pdfViewController;

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;

@end
