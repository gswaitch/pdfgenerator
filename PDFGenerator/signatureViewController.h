//
//  signatureViewController.h
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 25/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface signatureViewController : UIViewController

@property (nonatomic, strong) NSMutableArray *signatureArray;
@property (nonatomic, strong) UIImage *signatureImage;
@property (strong,nonatomic) NSString *signatureName;
@property (strong,nonatomic) NSString *date;

@property (nonatomic, strong) void (^signaturesProcessed)(NSArray *signatures);

- (instancetype)initWithSignature:(NSMutableArray*)signatures andProcessBlock:(void (^)(NSArray *signatures))signaturesProcessed;

- (instancetype)initWithSignatures:(NSMutableArray*)signatures;

@end
