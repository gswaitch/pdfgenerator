//
//  CIPDFViewController.m
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 21/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.


#import "CIPDFViewController.h"
#import "signatureViewController.h"
#import "ILPDFSignatureController.h"
#import "ImageCollectionViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface CIPDFViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@end

@implementation CIPDFViewController
{
    CIPDFController *pdfController;
    ILPDFViewController * pdfVC;
    ILPDFDocument *pdfDocument;
    ILPDFDocument *mergedDocument;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.imagesTaken = [NSMutableArray array];
    self.signaturesCollected = [NSMutableArray array];
    [self createPDFController];
    self.numberOfImagesPerPage = 2;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)createPDFController
{
    NSMutableArray *pdfs = [NSMutableArray array];
    NSArray *pdfArray = [NSArray arrayWithObjects: @"CMS-0000-HS-FM-003-A Hot Works Permit.pdf",@"MG Damage Form Template_Oct-17FINAL v4.1.pdf",@"CMS-3000-HS-FM-002-A Confined Space Permit (Victoria).pdf",@"testA.pdf",@"CMS-0000-MS-FM-006-C PTA Form- JamesExample.pdf", nil];
    for (NSString* pdfName in pdfArray)
    {
        NSString *pathWithExtension = pdfName;
        NSString *pdfPath = pathWithExtension.stringByDeletingPathExtension;
        
       NSString *path = [[NSBundle mainBundle] pathForResource:pdfPath ofType:@"pdf"];
        if (![path isEqualToString:@""])
        {
            NSData *pdfData = [NSData dataWithContentsOfFile:path];
            PDFQueueElement *element = [[PDFQueueElement alloc]initWithPdfName:pdfName andOriginalDocumentData:pdfData];
            [pdfs addObject:element];
        }
    }
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    pdfVC = [storyboard instantiateViewControllerWithIdentifier:@"pdfVC"];
    
    UIBarButtonItem *next = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Next"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(nextButtonPressed:)];
    
    UIBarButtonItem *camera = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(collectImages:)];
    
    UIBarButtonItem *signature = [[UIBarButtonItem alloc]
                                  initWithTitle:@"Sign"
                                  style:UIBarButtonItemStylePlain
                                  target:self
                                  action:@selector(collectSignatures:)];
                             
    NSArray *navigationButtonArray = [NSArray arrayWithObjects:next, camera,signature, nil];
    pdfVC.navigationItem.rightBarButtonItems = navigationButtonArray;
    
    [self.navigationController pushViewController:pdfVC animated:YES];

    pdfController = [[CIPDFController alloc]initWithPdfs:pdfs firstSourcePDFName:pdfArray[0] andPdfViewController:pdfVC];
    
    [pdfController go];
    
}


-(void)nextButtonPressed:(UIButton *)sender
{
    CGRect frame = CGRectMake (100,100, 80, 80);
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithFrame:frame];
    [pdfVC.view addSubview:self.activityIndicatorView];
    
    NSArray *requiredFields = [NSArray array];
    requiredFields = pdfVC.document.getRequiredFields;

    if (requiredFields.count == 0)
    {
        [pdfController next];
        [self showPreviousButton];
        
        bool next = pdfController.nextPdf;
        
        if (next == false)
        {
            [self.activityIndicatorView startAnimating];
            
            mergedDocument = [pdfController mergePDFToMakeFInalPDF];

            if (self.finalImages.count != 0)
            {
                mergedDocument = [pdfController combineImages:self.finalImages andumberOfImagesPerPage:self.numberOfImagesPerPage andDocumentToAppend:mergedDocument];
            }
            self.signaturesWithNameAndDate = [[NSMutableArray alloc]init];

            for(int i=0 ; i < self.finalSignatures.count ; i++)
            {
                NSString *stringToAdd =  [NSString stringWithFormat:@"%@\r\r%@", [[self.finalSignatures objectAtIndex:i] objectForKey:@"Name"],[[self.finalSignatures objectAtIndex:i] objectForKey:@"Date"]];
                UIImage *img;
                if (i==0)
                {
                    NSString *stringToAdd =  [NSString stringWithFormat:@"%@\r\r%@\r\r%@", @"                                              Signed By:",[[self.finalSignatures objectAtIndex:i] objectForKey:@"Name"],[[self.finalSignatures objectAtIndex:i] objectForKey:@"Date"]];
                    img = [self drawText:stringToAdd
                                 inImage:[[self.finalSignatures objectAtIndex:i] objectForKey:@"SigntureImage"]
                                 atPoint:CGPointMake(100,0)];
                    
                }
                else
                {
                    img = [self drawText:stringToAdd
                                 inImage:[[self.finalSignatures objectAtIndex:i] objectForKey:@"SigntureImage"]
                                 atPoint:CGPointMake(100,100)];
                }
                [self.signaturesWithNameAndDate addObject:img];
            }

            if (self.signaturesWithNameAndDate.count != 0)
            {
                mergedDocument = [pdfController combineImages:self.signaturesWithNameAndDate andDocumentToAppend:mergedDocument];
            }

            pdfVC.document = mergedDocument;
            
//            [self.activityIndicatorView stopAnimating];
        }
    }
    else
    {
    
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Required Fields" message:[NSString stringWithFormat:@"Please fill %@", requiredFields] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


-(void)showPreviousButton
{
    UIBarButtonItem *previous = [[UIBarButtonItem alloc]
                             initWithTitle:@"Previous"
                             style:UIBarButtonItemStylePlain
                             target:self
                             action:@selector(previousButtonPressed:)];
    
    pdfVC.navigationItem.leftBarButtonItem = previous;
}


-(void)previousButtonPressed:(UIButton *)sender
{
    [pdfController previous];
}


-(void)collectImages: (UIButton *)sender
{
    __weak typeof(self) weakSelf = self;
    ImageCollectionViewController *imageCollectionVC;
    imageCollectionVC = [[ImageCollectionViewController alloc]initWithPhotos:self.imagesTaken
                        andProcessBlock:^(NSArray<UIImage *> *images) {
    weakSelf.finalImages = images;

}];
    
    imageCollectionVC.modalPresentationStyle = UIModalPresentationFormSheet;
    [self.navigationController presentViewController:imageCollectionVC animated:YES completion:nil];
    
}


-(void)collectSignatures: (UIButton *)sender
{
    __weak typeof(self) weakSelf = self;

    signatureViewController *signatureVC = [[signatureViewController alloc]initWithSignature:self.signaturesCollected andProcessBlock:^(NSArray *signatures)
    {
        weakSelf.finalSignatures = signatures;

    }];

    signatureVC.modalPresentationStyle = UIModalPresentationFormSheet;
    [self.parentViewController presentViewController:signatureVC animated:YES completion:nil];

}


#pragma Mark - Addign text to Image

-(UIImage*) drawText:(NSString*) text inImage:(UIImage*) image atPoint:(CGPoint) point
{
    
    UIFont *font = [UIFont  systemFontOfSize:60];
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentLeft;
    NSDictionary *attribute = [NSDictionary dictionaryWithObject:style forKey:NSParagraphStyleAttributeName];
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(450,30,2000,300)];
    
    CGRect rect = CGRectMake(point.x, point.y, image.size.width + 100, image.size.height+30);
    [[UIColor blackColor] set];
    [text drawInRect:CGRectIntegral(rect) withAttributes:@{NSFontAttributeName:font, NSFontAttributeName: attribute}];

    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

@end
