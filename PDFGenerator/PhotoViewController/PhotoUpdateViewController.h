//
//  PhotoUpdateViewController.h
//  pdfForm
//
//  Created by Gagandeep Kaur Swaitch on 29/5/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ILPDFKit/ILPDFKit.h>
#import "IPhoto.h"
#import "PDFImageConverter.h"
#import "ImageCollectionViewController.h"


//#import "IPhoto.h"

@interface PhotoUpdateViewController : UIViewController
//@property (nonatomic, strong) id<IPhoto> photo;
@property (nonatomic) BOOL dataFieldsEnabled;
@property UIImage *image;
@property (nonatomic, weak) id<UIViewControllerPreviewingDelegate> delegate;
@property (nonatomic, copy) void (^imagedProcessed)(ILPDFDocument* annotatedPDF, UIImage *image, NSString *assetNumber, NSString *caption);

- (instancetype)initWithPhoto:(UIImage*)photo andProcessBlock:(void (^)(ILPDFDocument *annotatedPDF, UIImage *image,NSString *assetNumber, NSString *caption))imageProcessed;

- (instancetype)initWithPhoto:(UIImage*)photo;
@property (nonatomic, strong) id<IPhoto> photo;

- (NSString *)getFieldTitle;
- (void)disableViewController;
@end
