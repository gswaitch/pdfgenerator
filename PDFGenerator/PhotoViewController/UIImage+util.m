//
//  UIImage+util.m
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 5/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "UIImage+util.h"

@implementation UIImage (util)
- (BOOL)isLandscape;
{
    return (self.size.width > self.size.height);
}

@end
