//
//  ImageCollectionviewCell.h
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 30/5/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCollectionviewCell : UICollectionViewCell

typedef NS_ENUM(NSInteger, PhotoCellMode)
{
    PhotoCellModeNormal,
    PhotoCellModeDelete
};

@property (nonatomic, copy) void (^onDeleteButtonPressed)(ImageCollectionviewCell *cell);
@property (nonatomic) PhotoCellMode mode;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
