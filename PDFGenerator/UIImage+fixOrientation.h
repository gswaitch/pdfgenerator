//
//  UIImage+fixOrientation.h
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 3/7/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end
