//
//  ILPDFFormContainer+FormValues.m
//  ILPDFKit
//
//  Created by Gagandeep Kaur Swaitch on 21/5/18.
//

#import "ILPDFFormContainer+FormValues.h"

@implementation ILPDFFormContainer (FormValues)

-(NSDictionary *)getFormValues
{
    NSMutableDictionary *formValues;
    formValues = [NSMutableDictionary dictionary];
    
    for (ILPDFForm *form in self)
    {
        if(form.value != nil)
        {
            [formValues setObject:form.value forKey:form.name];
        }
    }
    
    return [formValues copy];
}


-(NSArray *) getRequiredFields
{
    NSMutableArray *fieldsToBeFilled = [[NSMutableArray alloc]init];
    for (ILPDFForm *form in self)
    {
        NSNumber *n = [form.dictionary objectForKey:@"Ff"];
        if([n isEqualToNumber:[NSNumber numberWithInt:2]])
        {
            if(form.value == nil)
            {
                [fieldsToBeFilled addObject:form.name];
                
            }
        }
        
    }
    return [fieldsToBeFilled copy];
}


-(NSDictionary *)getPDFNames
{
    NSMutableDictionary *pdfNames=[NSMutableDictionary dictionary];
    
    ILPDFForm *form = form;
    
    for (form in self)
    {
        NSString *pdfName = @"";
        NSData *pdfNameData = nil;

        if (form.formType == ILPDFFormTypeButton && (form.exportValue != nil && form.exportValue == form.value))
        {
            NSDictionary *aa = form.dictionary[@"AA"];

            for (NSString *key in aa)
            {
                pdfNameData = aa[key][@"F"][@"F"];
                
                if (pdfNameData != nil)
                {
                    break;
                }
            }

            if (pdfNameData != nil)
            {
                pdfName = [[pdfNameData utf8TextString] lastPathComponent];
            }
            
            [pdfNames setValue:pdfName forKey:form.name];
        }
    }
    
    return [pdfNames copy];
}

@end
