//
//  CIPDFController.h
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 15/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ILPDFKit/ILPDFKit.h>
#import "CIPDFViewController.h"
#import "PDFGenerator-Bridging-Header.h"


@interface PDFQueueElement : NSObject

@property (strong,nonatomic) NSString *sourcePdfName;
@property (strong,nonatomic) NSString *elementName;
@property (strong,nonatomic) NSString *targetPdfName;
@property (strong,nonatomic) ILPDFDocument *targetDocument;
@property (strong,nonatomic) NSData *originalDocumentData;


-(instancetype)initWithSourcePdfName:(NSString *)sourcePdfName elementName:(NSString *)elementName targetPdfName:(NSString *)targetPdfName andTargetDocument:(ILPDFDocument *)targetDocument;

-(instancetype)initWithPdfName:(NSString *)pdfName andOriginalDocumentData:(NSData *)originalDocumentData;

-(instancetype)initWithPdfQueueElement:(PDFQueueElement *)pdfQueueElement;

@end

@interface CIPDFController : NSObject
@property (strong,nonatomic) ILPDFViewController *pdfViewController;
@property (strong,nonatomic) NSMutableArray<PDFQueueElement *> *pdfRenderingQueue;
@property (nonatomic) NSUInteger currentIndex;
@property (strong,nonatomic) NSArray<PDFQueueElement *> *allPdfs;
@property (strong,nonatomic) NSString *firstSourcePDFName;
@property (strong,nonatomic) NSString *currentSourcePDFName;
@property (nonatomic, assign) BOOL nextPdf;
@property (strong,nonatomic) UIImage *combinedImage;
@property (strong,nonatomic) UIImage *combinedSignatureImage;
@property (strong,nonatomic) NSMutableArray *combinedImagesArray;
@property (strong,nonatomic) NSMutableArray *combinedSignatureArray;


@property (strong,nonatomic) NSMutableArray<PDFQueueElement *> *nextPdfs;

-(instancetype)initWithPdfs:(NSArray<PDFQueueElement *>*)allPdfs firstSourcePDFName:(NSString *)firstSourcePDFName andPdfViewController:(ILPDFViewController *)pdfViewController;

-(ILPDFDocument *)combineImages:(NSArray *)images andumberOfImagesPerPage:(NSUInteger )numberOfImagesPerPage andDocumentToAppend: (ILPDFDocument *)document;
-(ILPDFDocument *)combineImages:(NSArray *)images andDocumentToAppend: (ILPDFDocument *)document;

-(void)go;
-(void)next;
-(void)previous;
-(ILPDFDocument *)mergePDFToMakeFInalPDF;
@end
