//
//  ILPDFSignatureController.m
//  Pods
//
//  Created by Yuriy on 28/08/16.
//
//

#import "ILPDFSignatureController.h"
#import "ILPDFSignatureEditingView.h"
#import "ILPDFFormSignatureField.h"
#import "ILPDFForm.h"

@interface ILPDFSignatureController ()
@property (strong, nonatomic) IBOutlet ILPDFSignatureEditingView *signatureView;

@end

@implementation ILPDFSignatureController

//- (instancetype) initWithImage:(UIImage*) signatureImage
//{
//    self = [super init];
//    if(self)
//    {
//        self.signatureImage = signatureImage;
//    }
//    return self;
//}


- (void)viewDidLoad
{
    [super viewDidLoad];

    if (self.signatureImage != nil)
    {
        self.signatureView.incrementalImage = self.signatureImage;
    }
    
    self.view.backgroundColor = [UIColor clearColor];
    self.preferredContentSize=CGSizeMake(600, 300);
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Done"
                                   style:UIBarButtonItemStyleDone
                                   target:self
                                   action:@selector(signatureAction:)];
    
    UIBarButtonItem *clearButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Clear"
                                   style:UIBarButtonItemStyleDone
                                   target:self
                                   action:@selector(clearAction:)];
    self.navigationItem.rightBarButtonItem = doneButton;
    self.navigationItem.leftBarButtonItem = clearButton;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)clearAction:(UIButton *)sender
{
    
    [self.signatureView clearSignature];
    
}

- (IBAction)signatureAction:(UIButton *)sender
{
    self.signatureImage = [self.signatureView createImageFromSignWithMaxWidth:self.expectedSignSize.width andMaxHeight:self.expectedSignSize.height+60];
    
    [self.delegate signedWithImage:self.signatureImage];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
