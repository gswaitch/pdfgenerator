//
//  NSString+Utils.h
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 18/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <Foundation/Foundation.h>

#define StringIsEmpty(str) ((str==nil) || [str isEmpty])

@interface NSString (Utils)

- (BOOL)isEmpty;

@end
