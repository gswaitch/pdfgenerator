//
//  ILPDFFormContainer+FormValues.h
//  ILPDFKit
//
//  Created by Gagandeep Kaur Swaitch on 21/5/18.
//

#import <ILPDFKit/ILPDFKit.h>
#import "ILPDFFormContainer.h"

@interface ILPDFFormContainer (FormValues)


-(NSDictionary*) getFormValues;
-(NSDictionary*) getPDFNames;
-(NSArray*) getRequiredFields;

@end
